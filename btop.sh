#!/bin/bash

# Function to get the system architecture
get_arch() {
  case "$(uname -m)" in
    x86_64) echo "x86_64";;
    arm64) echo "aarch64";;
    aarch64) echo "aarch64";;
    *) echo "Unsupported architecture"; exit 1;;
  esac
}

# Get the system architecture
ARCH=$(get_arch)

# Install necessary tools if not already installed
sudo apt update
sudo apt install -y wget tar bzip2 jq

# Fetch the latest release version from GitHub API
LATEST_RELEASE=$(wget -qO- https://api.github.com/repos/aristocratos/btop/releases/latest | jq -r .tag_name)
echo $LATEST_RELEASE
# Construct the download URL
DOWNLOAD_URL="https://github.com/aristocratos/btop/releases/download/$LATEST_RELEASE/btop-$ARCH-linux-musl.tbz"

# Download the prebuilt binary from the releases page
wget $DOWNLOAD_URL -O btop.tbz

# Extract the tarball
tar -xjf btop.tbz

# Move the btop binary to /usr/local/bin
sudo mv btop/bin/btop /usr/local/bin/

# Set the correct permissions
sudo chmod +x /usr/local/bin/btop

# Clean up
rm -rf btop.tbz btop

# Verify installation
btop --version

echo "btop version $LATEST_RELEASE has been successfully installed."